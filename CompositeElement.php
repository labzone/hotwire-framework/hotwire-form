<?php

namespace HotWire\Form;

abstract class CompositeElement
{
    protected $elements=array();

    public function addElement(IElement $element)
    {
        $this->elements[$element->getName()]=$element;

        return $this;
    }

    public function remove(IElement $element)
    {
        if (isset($this->elements[$element->getName()])) {
            unset($this->elements[$element->getName()]);
        }

        return $this;
    }

    public function getChildren()
    {
        return $this->elements;
    }
}
