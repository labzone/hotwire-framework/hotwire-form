<?php

namespace HotWire\Form\DependencyInjection;

use HotWire\DependencyInjection\Extension;
use HotWire\Form\Field\TextField;
use HotWire\Form\Field\DateField;
use HotWire\Form\Field\CheckBoxField;
use HotWire\Form\Field\ButtonField;
use HotWire\Form\Field\HiddenField;
use HotWire\Form\Field\ListField;
use HotWire\Form\Field\RadioButtonField;
use HotWire\Form\Builder\FormBuilder;
use HotWire\Form\Field\TextareaField;

class FormExtension extends Extension
{
    public function load()
    {
        $this->container->register('text.field', new TextField())
                        ->register('date.field', new DateField())
                        ->register('hidden.field', new HiddenField())
                        ->register('button.field', new ButtonField())
                        ->register('checkBox.field', new CheckBoxField())
                        ->register('list.field', new ListField())
                        ->register('radio.field', new RadioButtonField())
                        ->register('textarea.field', new TextareaField())
                        ->register('form.builder', new FormBuilder());

        return $this;
    }
}
