<?php

namespace HotWire\Form;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    public function getName()
    {
        return 'HotWire:Form';
    }
}
