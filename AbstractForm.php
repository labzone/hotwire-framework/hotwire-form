<?php

namespace HotWire\Form;

use HotWire\DependencyInjection\Container;
use HotWire\Form\Builder\IBuilder;
use HotWire\Http\Request;
use HotWire\Util\Collection\ArrayList;

abstract class AbstractForm
{
    protected $data;

    public function render()
    {
        $builder=$this->get('form.builder');
        $this->build($builder);

        return $builder->render();
    }

    public function handle(Request $request, $object)
    {
        if (!is_object($object)) {
            return;
        }
        $data=null;
        if ($request->isPost()) {
            $data=$request->request();
        } else {
            $data=$request->query();
        }
        if ($data instanceof ArrayList) {
            $reflectionClass=new \ReflectionClass($object);
            foreach ($data->getItems() as $key => $item) {
                if ($reflectionClass->hasProperty($key)) {
                    if ($propertInfo=$reflectionClass->getProperty($key)) {
                        $propertInfo->setAccessible(true);
                        $propertInfo->setValue($object, $item);
                    }
                }
            }
        }

        return $object;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data=$data;

        return $this;
    }

    private function get($name)
    {
        return Container::getInstance()->get($name);
    }

    abstract public function build(IBuilder $builder);
}
