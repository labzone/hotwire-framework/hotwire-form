<?php

namespace HotWire\Form\Builder;

use HotWire\DependencyInjection\Container;
use HotWire\Form\IElement;
use HotWire\Form\CompositeElement;

class FormBuilder extends CompositeElement implements IBuilder, IElement
{
    private $method='post';
    private $action;

    public function setMethod($method)
    {
        if (in_array(strtolower($method), array('get','post'))) {
            $this->method=$method;
        }

        return $this;
    }

    public function setAction($action)
    {
        $this->action=$action;

        return $this;
    }

    final public function add($name, $fieldType = 'text', $attributes = array())
    {
        if (!$fieldType) {
            $fieldType='text';
        }
        if ($field=$this->get("{$fieldType}.field")) {
            $field->setName($name)
                  ->setAttributes($attributes);
            $this->addElement(clone $field);
        }

        return $this;
    }

    public function get($name)
    {
        return Container::getInstance()->get($name);
    }

    public function render()
    {
        $fields=array();
        foreach ($this->elements as $element) {
            $fields[]=$element->render();
        }
        $parameters=array(
            'fields'=>$fields,
            'action'=>$this->action,
            'method'=>$this->method
        );
        $templating=Container::getInstance()->get('templating.twig');

        return $templating->setView("HotWire:Form::Form/Form")
                          ->setParameters($parameters)
                          ->render()->getContent();
    }

    public function getName()
    {
        return 'form';
    }
}
