<?php

namespace HotWire\Form\Builder;

interface IBuilder
{
    public function add($name, $fieldType = null);
}
