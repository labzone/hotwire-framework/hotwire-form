<?php

namespace HotWire\Form\Field;

class ButtonField extends AbstractField
{
    public function __construct()
    {
        $this->setValue('Submit')
             ->setType('submit')
        ;
    }

    public function setType($type)
    {
        $this->type=$type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
}
