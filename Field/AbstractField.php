<?php

namespace HotWire\Form\Field;

use HotWire\DependencyInjection\Container;
use HotWire\Form\IElement;

abstract class AbstractField implements IElement
{
    protected $value;
    protected $type;
    protected $name;
    protected $attributes=array();

    private function getShortName()
    {
        $reflectionClass=new \ReflectionClass($this);

        return $reflectionClass->getShortName();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name=$name;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value=$value;

        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes=$attributes;

        return $this;
    }

    final public function render()
    {
        $parameters=array(
            'name'=>$this->name,
            'type'=>$this->type,
            'value'=>$this->value,
        );
        $templating=Container::getInstance()->get('templating.twig');

        return $templating->setView("HotWire:Form/Field/{$this->getShortName()}")
                          ->setParameters(array_merge($parameters, $this->attributes))
                          ->render()->getContent();
    }
}
