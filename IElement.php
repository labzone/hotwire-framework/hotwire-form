<?php

namespace HotWire\Form;

interface IElement
{
    public function getName();
}
